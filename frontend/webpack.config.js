var path = require('path');


var config = {
    src: path.join(__dirname, 'src'),
    assets: path.join(__dirname, 'assets'),
    css: path.join(__dirname, 'assets', 'css'),
    js: path.join(__dirname, 'assets', 'js'),
    other: path.join(__dirname, 'assets', 'other'),
    build: path.join(__dirname, 'build')
};


module.exports = {
    mode: 'development',
    context: __dirname,
    entry: path.join(config.src, 'index.js'),
    output: {
        path: config.build,
        publicPath: '/static/',
        filename: 'js/[name].js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                // loader: 'babel-loader',
                // include: [config.src],
                // query: {
                //     presets: ['react', 'es2015']
                // },
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    },
    // module: {
    //     loaders: [
    //         {
    //             test: /\.jsx?$/,
    //             loader: 'babel-loader',
    //             include: [config.src],
    //             query: {
    //                 presets: ['react', 'es2015']
    //             }
    //         }
    //     ]
    // },
    // resolve: {
    //     extensions: ['.js', '.less', '.jsx', '.css'],
    //     modules: [
    //         path.resolve(config.src),
    //         "node_modules"
    //     ]
    // }
};