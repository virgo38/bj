var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'production';

var config = {
        src: path.join(__dirname, 'src'),
        assets: path.join(__dirname, 'assets'),
        css: path.join(__dirname, 'assets', 'css'),
        js: path.join(__dirname, 'assets', 'js'),
        other: path.join(__dirname, 'assets', 'other'),
        build: path.join(__dirname, 'build')
    };

var settings = {
    context: __dirname,
    entry: [
        path.join(config.src, 'main'),
        path.join(__dirname, 'assets', 'css', 'styles', 'style'),
        path.join(__dirname, 'assets', 'img', 'index'),
        path.join(__dirname, 'assets', 'fonts', 'index'),
        path.join(__dirname, 'assets', 'other', 'index')
    ],
    output: {
        path: config.build,
        publicPath: '/static/',
        filename: 'js/[name].js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                include: [config.src],
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {
                test: /\.less$/,
                include: config.assets,
                loader: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader!less-loader"})
            },
            {
                test: /(\.(jpg|png|svg))|(manifest.json)|(browserconfig.xml)$/,
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[ext]'
                }
            },
            {
                test: /(\.(woff2|ttf|woff|eot))|(_font\.svg)$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            },
            {
                test: /(robots.txt)|(sitemap.xml)$/,
                loader: 'file-loader',
                include: [config.other],
                options: {
                    name: '[name].[ext]'
                }
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin({
            filename: "css/style.css"
        }),
        new webpack.ProvidePlugin({
            'window.jQuery': 'jquery',
            'window.$': 'jquery',
            'jQuery': 'jquery',
            '$': 'jquery'
        }),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        })
    ],
    resolve: {
        extensions: ['.js', '.less', '.jsx', '.css'],
        modules: [
            path.resolve(config.src),
            "node_modules"
        ]
    },
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: NODE_ENV == 'development' ? "cheap-inline-module-source-map" : "source-map"
};

if (NODE_ENV == 'production') {
    settings.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                // don't show unreachable variables etc
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}

module.exports = settings;
